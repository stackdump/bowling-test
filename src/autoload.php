<?php

class AutoLoader {

    //REVIEW is there a standard way load only certain classes
    public static function loadClass($class_name)
    {
        $file =  __DIR__ . DIRECTORY_SEPARATOR .  str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';

        if(file_exists($file))
            include $file;
    }
}

spl_autoload_register(array('AutoLoader', 'loadClass'));

