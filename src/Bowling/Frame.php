<?php

namespace Bowling;

class Frame
{
    protected $rolls;

    public function __construct()
    {
        $this->rolls = [];
    }
    
    public function nextFrame()
    {
        if ( ! $this->isOpen())
            return true;
        elseif ( $this->isOpen() && $this->score() == 10 )
            return true;
        return false;
    }

    public function isOpen()
    {
        if (sizeof($this->rolls) <= 1)
            return true;
        elseif ($this->score() == 10 && sizeof($this->rolls) == 2) # spare
            return true;
        elseif ($this->score() == 10 && sizeof($this->rolls) == 1) # strike
            return true;

        return false;
    }

    public function roll($pins)
    {
        if (! $this->isOpen())
            throw new Exception('frame not open');
        elseif ($pins > 10)
            throw new Exception('invalid pin count');
        elseif (($pins + $this->score() > 10) && sizeof($this->rolls) == 1)
            throw new Exception('invalid pin count');
        elseif (($pins + $this->score() < 10) && sizeof($this->rolls) == 2)
            throw new Exception('invalid roll - require strike/spare');
        else
            array_push($this->rolls, $pins);
    }

    public function score()
    {
        return array_sum($this->rolls);
    }
}
