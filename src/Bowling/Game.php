<?php

namespace Bowling;

class Game
{
    protected $frames;
    protected $currentFrame;

    public function __construct()
    {
        $this->frames = [];
        $this->current = 0;
        $this->frames[0] = new Frame;
    }

    public function currentFrame()
    {
        return $this->frames[$this->current];
    }

    // Called for each roll of the bowling ball. Passed the number
    // of pins knocked down on this roll.
    public function roll($pins)
    {
        if (! $this->isOpen())
            throw new Exception('game not open');

        # REVIEW: maybe add try/catch here
        $f = $this->currentFrame();
        if ( $f->nextFrame())
        {
            $this->current++;
            $this->frames[$this->current] = new Frame;
            $f = $this->currentFrame();
        }

        # TODO: add score to previous frame on strike/spare
        $f->roll($pins);
    }

    public function isOpen()
    {
        # TODO allow for extra frames
        return (sizeof($this->frames) <= 10);
    }

    // Called once at the end of the game.
    public function score()
    {
        $sum = 0;
        foreach( $this->frames as $frame) {
            $sum += $frame->score();
        }

        return $sum;
    }
}
