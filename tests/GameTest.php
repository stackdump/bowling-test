<?php

use \Bowling\Game as Game;

class GameTest extends PHPUnit_Framework_TestCase
{
    #public function testGutterBallGame()
    #{
    #    $game = new Game;

    #    // 10 frames, 2 rolls each = 20 gutter balls.
    #    for ($i = 0; $i < 20; $i++) {
    #        $game->roll(0);
    #    }

    #    // End of the game!
    #    $this->assertEquals(0, $game->score());
    #}

    #public function testOneEveryTime()
    #{
    #    $game = new Game;

    #    // 10 frames, 2 rolls each = 20 gutter balls.
    #    for ($i = 0; $i < 20; $i++) {
    #        $game->roll(1);
    #    }

    #    // End of the game!
    #    $this->assertEquals(20, $game->score());
    #}

    public function testPerfectGame()
    {
        $game = new Game;

        for ($i = 0; $i < 12; $i++) {
            echo "roll" . $i . "\n";
            $game->roll(10);
        }

        $this->assertEquals(300, $game->score());
    }
    
}
