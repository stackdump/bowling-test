<?php

use \Bowling\Frame as Frame;

class FrameTest extends PHPUnit_Framework_TestCase
{
    public function testFrameIsOpen()
    {
        $frame = new Frame;
        $this->assertTrue($frame->isOpen());
    }

    public function testFrameScore()
    {
        $frame = new Frame;
        $frame->roll(8);
        $frame->roll(2);
        //TODO add spare logic
        $this->assertEquals($frame->score(), 10);
    }

   /**
    * @expectedException \Bowling\Exception
    */
    public function testScoreValidation()
    {
        $frame = new Frame;
        $frame->roll(8);
        $frame->roll(4);
    }

   /**
    * @expectedException \Bowling\Exception
    */
    public function testTenPinsOnly()
    {
        $frame = new Frame;
        $frame->roll(18);
    }

    public function testFrameCloses()
    {
        $frame = new Frame;
        $frame->roll(5);
        $frame->roll(3);
        $this->assertFalse($frame->isOpen());
        $this->assertEquals($frame->score(), 8);
    }

    public function testCanRollSpare()
    {
        $frame = new Frame;
        $frame->roll(5);
        $frame->roll(5);
        $this->assertTrue($frame->isOpen());
        $this->assertEquals($frame->score(), 10);
        $frame->roll(5);
        $this->assertEquals($frame->score(), 15);
        $this->assertFalse($frame->isOpen());
    }

    public function testCanRollStrike()
    {
        $frame = new Frame;
        $frame->roll(10);
        $this->assertTrue($frame->nextFrame());
    }
}
